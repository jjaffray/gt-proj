# plays quiet if either opponent played quiet, confess oth.
module CO
  class GenerousT4T
    def play(n, my_history, opp1_history, opp2_history)
      if n.zero?
        :quiet
      else
        if opp1_history.last == :quiet || opp2_history.last == :quiet
          :quiet
        else
          :confess
        end
      end
    end
  end
end
