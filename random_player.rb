module CO
  class RandomPlayer
    def play(n, my_history, opp1_history, opp2_history)
      [:quiet, :confess].sample
    end
  end
end
