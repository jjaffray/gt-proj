module CO
  class Runner
    attr_reader :p1, :p2, :p3
    attr_reader :histories
    attr_reader :round
    def initialize(p1, p2, p3)
      @p1 = p1
      @p2 = p2
      @p3 = p3
      @histories = [[], [], []]
      @round = 0
    end

    def play_rounds(n)
      n.times { play_round }
    end

    def play_round
      p1_result = p1.play(round, histories[0], histories[1], histories[2])
      p2_result = p2.play(round, histories[1], histories[2], histories[0])
      p3_result = p3.play(round, histories[2], histories[0], histories[1])
      @round += 1
      histories[0] << p1_result
      histories[1] << p2_result
      histories[2] << p3_result
    end

    def scores
      scores = [0, 0, 0]
      histories.transpose.each do |a,b,c|
        scores = scores.zip(payout_profile(a, b, c)).map { |a, b| a + b }
      end
      scores.map { |x| Float(x)/@round }
    end

    def payout_profile(a,b,c)
      case [a,b,c]
      when [:quiet, :quiet, :quiet]
        [6, 6, 6]
      when [:quiet, :quiet, :confess]
        [3, 3, 8]
      when [:quiet, :confess, :quiet]
        [3, 8, 3]
      when [:quiet, :confess, :confess]
        [0, 5, 5]
      when [:confess, :quiet, :quiet]
        [8, 3, 3]
      when [:confess, :quiet, :confess]
        [5, 0, 5]
      when [:confess, :confess, :quiet]
        [5, 5, 0]
      when [:confess, :confess, :confess]
        [2, 2, 2]
      end
    end
  end
end
