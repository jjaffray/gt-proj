require "pry"

require_relative "./game_runner"
require_relative "./nice_player"
require_relative "./mean_player"
require_relative "./random_player"
require_relative "./tolerant_player"
require_relative "./freaky_player"
require_relative "./t4t"
require_relative "./generous_t4t"

module CO
  players = [NicePlayer, MeanPlayer, RandomPlayer, TolerantPlayer, T4T, GenerousT4T]
  scores = Hash.new { |h, k| h[k] = [] }
  rounds = Hash.new { |h, k| h[k] = 0 }
  total_rounds = 0
  players.repeated_permutation(3).each do |player1, player2, player3|
    p1 = player1.new
    p2 = player2.new
    p3 = player3.new
    runner = Runner.new(p1, p2, p3)
    runner.play_rounds(100)
    result = runner.scores

    scores[player1] << result[0]
    scores[player2] << result[1]
    scores[player3] << result[2]
    rounds[player1] += 1
    rounds[player2] += 1
    rounds[player3] += 1
  end
  # each entry in rounds should be the same...
  each_rounds = rounds.first.last
  puts "Each player played #{each_rounds} rounds"
  p scores.map { |k, v| [k, (v.inject(&:+) / each_rounds).round(3)] }
end
