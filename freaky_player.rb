module CO
  class FreakyPlayer
    def initialize
      @strategy = [:quiet, :confess].sample
    end
    def play(n, my_history, opp1_history, opp2_history)
      @strategy
    end
  end
end
