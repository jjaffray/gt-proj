module CO
  class TolerantPlayer
    def play(n, my_history, opp1_history, opp2_history)
      histories = opp1_history + opp2_history
      if histories.select { |x| x == :quiet }.count > 
         histories.select { |x| x == :confess }.count
         :quiet
      else
        :confess
      end
    end
  end
end
